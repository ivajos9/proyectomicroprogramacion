;Grupo#16   Andrea Gudiel , Ivan Diaz 
.model small
.stack
.data
	carv db '*' ;caracter vertical
	carh db '%' ;caracter horizontal
	carinit db '#' ; caracter que marcara el inicio
	sep db '-$'
	col db 15
	row db 15
	msg0 db 10,13,7,'Ingresa un numero: ','$'
	msg1 db 10,13,7,'Posiciones: ','$'
	pare1 db '($'
	coma db ',$'
	pare2 db ') $'
	;unidades decenas y centenas
	ce db 0
	de db 0
	un db 0
	max db 38 ; valor maximo
	cont db 25
	counter db 1 ; lleva el numero por el que va
	turn dw 1 ; controla cuantas veces debe hacer el ciclo
.code
main:
	mov ax, @data
    mov ds, ax
    mov es, ax
	;
	
	; lectura de numeros
    mov ah, 09h
    lea dx, msg0
    int 21h
	
	;captura la centena
    mov ah, 01h
    int 21h
    sub al, 30h
    mov ce, al
    
    ;captura la decena
    mov ah, 01h
    int 21h
    sub al, 30h
    mov de, al
	
	;captura la unidad
	mov ah, 01h
    int 21h
    sub al, 30h
    mov un, al
	
	;suma de decenas a unidades
	mov al, ce
	mov bl, 100
	mul bl
	mov max, al
	
	;suma de decenas a unidades
	mov al, de
	mov bl, 10
	mul bl
	add al, un
	add al, max
	mov max, al
	
	;imprimir caracter inicial
	mov ah, 2
	mov dh, row
	mov dl, col
	mov bh, 0
	int 10h
	mov ah, 2
	mov dl, carinit
	int 21h

	;brinca a comparacion
    jmp comparar
    general:
	;movimiento a la derecha
	mov cx, turn
	right:
	inc col
	inc col
	mov ah, 2
	mov dh, row
	mov dl, col
	mov bh, 0
	int 10h
	inc counter
	;imprimir caracter
	mov ah, 2
	mov dl, carh
	int 21h
	;Comparar antes de recorrer
	mov al, counter
	cmp al, max    ; Verifica si aun es menor
	jz listar
	loop right
	
	; Movimiento hacia arriba
	mov cx, turn
	up:
	dec row
	dec row
	mov ah, 2
	mov dh, row
	mov dl, col
	mov bh, 0
	int 10h
	inc counter
	;imprimir caracter
	mov ah, 2
	mov dl, carv
	int 21h
	;Comparar antes de recorrer
	mov al, counter
	cmp al, max    ; Verifica si aun es menor
	jz listar
	
	loop up
	inc turn ; detecta un cruce
	
	;movimiento a la izq
	mov cx, turn
	left:
	dec col
	dec col
	mov ah, 2
	mov dh, row
	mov dl, col
	mov bh, 0
	int 10h
	inc counter
	;imprimir caracter
	mov ah, 2
	mov dl, carh
	int 21h
	;Comparar antes de recorrer
	mov al, counter
	cmp al, max    ; Verifica si aun es menor
	jz listar
	loop left
	
	;movimiento hacia abajo
	mov cx, turn
	down:
	inc row
	inc row
	mov ah, 2
	mov dh, row
	mov dl, col
	mov bh, 0
	int 10h
	inc counter
	;imprimir caracter
	mov ah, 2
	mov dl, carv
	int 21h
	;Comparar antes de recorrer
	mov al, counter
	cmp al, max    ; Verifica si aun es menor
	jz listar
	loop down
	inc turn ;detecta un cruce
	
	comparar:
	mov al, counter
	cmp al, max    ; verifica si debe continuar
    jc irgeneral 
	jz listar ;termina el proceso
	jnz listar ; termina el proceso
	
	irgeneral:
	jmp general
	
	;########################## LISTAR 
	listar:
	;Inicia listado
	mov counter, 1
	mov row, 0
	mov col, 1
	mov turn, 1
	inc max
	;imprime mensaje de listado
	mov ah, 2
	mov dh, cont
	mov dl, 1
	mov bh, 0
	int 10h
	
	mov ah, 09h
    lea dx, msg1
	int 21h
	mov DL, 0Ah	   ; Imprimir Enter
    mov AH, 02h
    int 21h
	inc cont
	inc cont
	
	;ciclo de escritura
	mov ah, 2
	mov dh, cont
	mov dl, 1
	mov bh, 0
	int 10h
	
	mov al,counter ; asigno un valor de 3 digitos en decimal al registro AL
    aam ;ajusta el valor en AL por: AH=23 Y AL=4
    mov un,al ; Respaldo 4 en unidades
    mov al,ah ;muevo lo que tengo en AH a AL para poder volver a separar los números
    aam ; separa lo qe hay en AL por: AH=2 Y AL=3
    mov ce,ah ;respaldo las centenas en cen en este caso 2
    mov de,al ;respaldo las decenas en dec, en este caso 3
	
    ;Imprimos los tres valores empezando por centenas, decenas y unidades.
    mov ah,02h
    mov dl,ce
    add dl,30h ; se suma 30h a dl para imprimir el numero real.
    int 21h

    mov dl,de
    add dl,30h
    int 21h

    mov dl,un
    add dl,30h
    int 21h
	inc counter
	;imprimir numero y posicion inicial
	mov DX, offset sep
    mov AH, 09h    ;parametro para imprimir cadenas
    int 21h        ;ejecuta la interrupcion
	
	mov DX, offset pare1
    mov AH, 09h    ;parametro para imprimir cadenas
    int 21h        ;ejecuta la interrupcion
	
	mov al,col
    aam
    mov bx,ax
    mov ah,02h
    mov dl,bh
    add dl,30h
    int 21h
    
    mov ah,02h
    mov dl,bl
    add dl,30h
    int 21h
	
	mov DX, offset coma
    mov AH, 09h    ;parametro para imprimir cadenas
    int 21h        ;ejecuta la interrupcion
	
	mov al,row
    aam
    mov bx,ax
    mov ah,02h
    mov dl,bh
    add dl,30h
    int 21h
    
    mov ah,02h
    mov dl,bl
    add dl,30h
    int 21h
	
	mov DX, offset pare2
    mov AH, 09h    ;parametro para imprimir cadenas
    int 21h        ;ejecuta la interrupcion
	
	inc cont
	mov DL, 0Ah	   ; Imprimir Enter
    mov AH, 02h
    int 21h
	;ciclo de escritura
	mov ah, 2
	mov dh, cont
	mov dl, 1
	mov bh, 0
	int 10h

    ;###########################Ciclo de escritura
	;brinca a comparacion
    jmp comparar2
    escribir:
	;movimiento a la derecha
	mov cx, turn
	derecha:
	inc col
	; Imprimir numero 
	mov al,counter ; asigno un valor de 3 digitos en decimal al registro AL
    aam ;ajusta el valor en AL por: AH=23 Y AL=4
    mov un,al ; Respaldo 4 en unidades
    mov al,ah ;muevo lo que tengo en AH a AL para poder volver a separar los números
    aam ; separa lo qe hay en AL por: AH=2 Y AL=3
    mov ce,ah ;respaldo las centenas en cen en este caso 2
    mov de,al ;respaldo las decenas en dec, en este caso 3

    ;Imprimos los tres valores empezando por centenas, decenas y unidades.
    mov ah,02h
    mov dl,ce
    add dl,30h ; se suma 30h a dl para imprimir el numero real.
    int 21h

    mov dl,de
    add dl,30h
    int 21h

    mov dl,un
    add dl,30h
    int 21h
	
	mov DX, offset sep
    mov AH, 09h    ;parametro para imprimir cadenas
    int 21h        ;ejecuta la interrupcion
	
	mov DX, offset pare1
    mov AH, 09h    ;parametro para imprimir cadenas
    int 21h        ;ejecuta la interrupcion
	
	mov al,col
    aam
    mov bx,ax
    mov ah,02h
    mov dl,bh
    add dl,30h
    int 21h
    
    mov ah,02h
    mov dl,bl
    add dl,30h
    int 21h
	
	mov DX, offset coma
    mov AH, 09h    ;parametro para imprimir cadenas
    int 21h        ;ejecuta la interrupcion
	
	mov al,row
    aam
    mov bx,ax
    mov ah,02h
    mov dl,bh
    add dl,30h
    int 21h
    
    mov ah,02h
    mov dl,bl
    add dl,30h
    int 21h
	
	mov DX, offset pare2
    mov AH, 09h    ;parametro para imprimir cadenas
    int 21h        ;ejecuta la interrupcion

	
	inc counter
	;Comparar antes de recorrer
	mov al, counter
	cmp al, max    ; Verifica si aun es menor
	jz terminar
	loop derecha
	
	inc cont
	mov DL, 0Ah	   ; Imprimir Enter
    mov AH, 02h
    int 21h
	;ciclo de escritura
	mov ah, 2
	mov dh, cont
	mov dl, 1
	mov bh, 0
	int 10h
	
	; Movimiento hacia arriba
	mov cx, turn
	arriba:
	inc row
	mov al,counter ; asigno un valor de 3 digitos en decimal al registro AL
    aam ;ajusta el valor en AL por: AH=23 Y AL=4
    mov un,al ; Respaldo 4 en unidades
    mov al,ah ;muevo lo que tengo en AH a AL para poder volver a separar los números
    aam ; separa lo qe hay en AL por: AH=2 Y AL=3
    mov ce,ah ;respaldo las centenas en cen en este caso 2
    mov de,al ;respaldo las decenas en dec, en este caso 3
	
    ;Imprimos los tres valores empezando por centenas, decenas y unidades.
    mov ah,02h
    mov dl,ce
    add dl,30h ; se suma 30h a dl para imprimir el numero real.
    int 21h

    mov dl,de
    add dl,30h
    int 21h

    mov dl,un
    add dl,30h
    int 21h
	mov DX, offset sep
    mov AH, 09h    ;parametro para imprimir cadenas
    int 21h        ;ejecuta la interrupcion
	
	mov DX, offset pare1
    mov AH, 09h    ;parametro para imprimir cadenas
    int 21h        ;ejecuta la interrupcion
	
	mov al,col
    aam
    mov bx,ax
    mov ah,02h
    mov dl,bh
    add dl,30h
    int 21h
    
    mov ah,02h
    mov dl,bl
    add dl,30h
    int 21h
	
	mov DX, offset coma
    mov AH, 09h    ;parametro para imprimir cadenas
    int 21h        ;ejecuta la interrupcion
	
	mov al,row
    aam
    mov bx,ax
    mov ah,02h
    mov dl,bh
    add dl,30h
    int 21h
    
    mov ah,02h
    mov dl,bl
    add dl,30h
    int 21h
	
	mov DX, offset pare2
    mov AH, 09h    ;parametro para imprimir cadenas
    int 21h        ;ejecuta la interrupcion


	inc counter
	;Comparar antes de recorrer
	mov al, counter
	cmp al, max    ; Verifica si aun es menor
	jz terminar
	
	loop arriba
	inc turn ; detecta un cruce
	
	inc cont
	mov DL, 0Ah	   ; Imprimir Enter
    mov AH, 02h
    int 21h
	;ciclo de escritura
	mov ah, 2
	mov dh, cont
	mov dl, 1
	mov bh, 0
	int 10h
	
	;movimiento a la izq
	mov cx, turn
	izquierda:
	dec col
	mov al,counter ; asigno un valor de 3 digitos en decimal al registro AL
    aam ;ajusta el valor en AL por: AH=23 Y AL=4
    mov un,al ; Respaldo 4 en unidades
    mov al,ah ;muevo lo que tengo en AH a AL para poder volver a separar los números
    aam ; separa lo qe hay en AL por: AH=2 Y AL=3
    mov ce,ah ;respaldo las centenas en cen en este caso 2
    mov de,al ;respaldo las decenas en dec, en este caso 3
	
    ;Imprimos los tres valores empezando por centenas, decenas y unidades.
    mov ah,02h
    mov dl,ce
    add dl,30h ; se suma 30h a dl para imprimir el numero real.
    int 21h

    mov dl,de
    add dl,30h
    int 21h

    mov dl,un
    add dl,30h
    int 21h
	mov DX, offset sep
    mov AH, 09h    ;parametro para imprimir cadenas
    int 21h        ;ejecuta la interrupcion
	
	mov DX, offset pare1
    mov AH, 09h    ;parametro para imprimir cadenas
    int 21h        ;ejecuta la interrupcion
	
	mov al,col
    aam
    mov bx,ax
    mov ah,02h
    mov dl,bh
    add dl,30h
    int 21h
    
    mov ah,02h
    mov dl,bl
    add dl,30h
    int 21h
	
	mov DX, offset coma
    mov AH, 09h    ;parametro para imprimir cadenas
    int 21h        ;ejecuta la interrupcion
	
	mov al,row
    aam
    mov bx,ax
    mov ah,02h
    mov dl,bh
    add dl,30h
    int 21h
    
    mov ah,02h
    mov dl,bl
    add dl,30h
    int 21h
	
	mov DX, offset pare2
    mov AH, 09h    ;parametro para imprimir cadenas
    int 21h        ;ejecuta la interrupcion
	
	inc counter
	;Comparar antes de recorrer
	mov al, counter
	cmp al, max    ; Verifica si aun es menor
	jz terminar
	loop izquierda
	
	inc cont
	mov DL, 0Ah	   ; Imprimir Enter
    mov AH, 02h
    int 21h
	;ciclo de escritura
	mov ah, 2
	mov dh, cont
	mov dl, 1
	mov bh, 0
	int 10h
	
	;movimiento hacia abajo
	mov cx, turn
	abajo:
	dec row
	mov al,counter ; asigno un valor de 3 digitos en decimal al registro AL
    aam ;ajusta el valor en AL por: AH=23 Y AL=4
    mov un,al ; Respaldo 4 en unidades
    mov al,ah ;muevo lo que tengo en AH a AL para poder volver a separar los números
    aam ; separa lo qe hay en AL por: AH=2 Y AL=3
    mov ce,ah ;respaldo las centenas en cen en este caso 2
    mov de,al ;respaldo las decenas en dec, en este caso 3
	
    ;Imprimos los tres valores empezando por centenas, decenas y unidades.
    mov ah,02h
    mov dl,ce
    add dl,30h ; se suma 30h a dl para imprimir el numero real.
    int 21h

    mov dl,de
    add dl,30h
    int 21h

    mov dl,un
    add dl,30h
    int 21h
	mov DX, offset sep
    mov AH, 09h    ;parametro para imprimir cadenas
    int 21h        ;ejecuta la interrupcion
	
	mov DX, offset pare1
    mov AH, 09h    ;parametro para imprimir cadenas
    int 21h        ;ejecuta la interrupcion
	
	mov al,col
    aam
    mov bx,ax
    mov ah,02h
    mov dl,bh
    add dl,30h
    int 21h
    
    mov ah,02h
    mov dl,bl
    add dl,30h
    int 21h
	
	mov DX, offset coma
    mov AH, 09h    ;parametro para imprimir cadenas
    int 21h        ;ejecuta la interrupcion
	
	mov al,row
    aam
    mov bx,ax
    mov ah,02h
    mov dl,bh
    add dl,30h
    int 21h
    
    mov ah,02h
    mov dl,bl
    add dl,30h
    int 21h
	
	mov DX, offset pare2
    mov AH, 09h    ;parametro para imprimir cadenas
    int 21h        ;ejecuta la interrupcion
	
	inc counter
	;Comparar antes de recorrer
	mov al, counter
	cmp al, max    ; Verifica si aun es menor
	jz terminar
	loop abajo
	inc turn ;detecta un cruce
	
	inc cont
	mov DL, 0Ah	   ; Imprimir Enter
    mov AH, 02h
    int 21h
	;ciclo de escritura
	mov ah, 2
	mov dh, cont
	mov dl, 1
	mov bh, 0
	int 10h
	
	comparar2:
	mov al, counter
	cmp al, max    ; verifica si debe continuar
    jc irescribir 
	jz terminar ;termina el proceso
	jnz terminar ; termina el proceso
	
	irescribir:
	jmp escribir
	
	terminar: 
	mov ax, 4c00h
	int 21h
end